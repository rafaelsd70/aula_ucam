import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:woocommerce/woocommerce.dart';

class ProdutosPorCategoria extends StatelessWidget {
  final int idCategoria;
  final WooCommerce wooCommerce;
  ProdutosPorCategoria(this.idCategoria, this.wooCommerce);

  @override
  Widget build(BuildContext context) {
    var scaffold = Scaffold(
      appBar: AppBar(title: Text("Produtos de Basketball")),
      body: SingleChildScrollView(
        child: FutureBuilder(
          future: wooCommerce.getProducts(category: idCategoria.toString()),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              List<WooProduct> produtos = snapshot.data as List<WooProduct>;
              List<Widget> listaProdutos = [];
              print(produtos.length);
              produtos.forEach((produto) {
                listaProdutos.add(ListTile(
                  title: Text(produto.name),
                  subtitle: Column(
                    children: [Text(produto.description), Text(produto.price)],
                  ),
                ));
              });
              return Column(
                children: listaProdutos,
              );
            } else {
              snapshot.error as String;
              return Text(snapshot.error as String);
            }
          },
        ),
      ),
    );
    return scaffold;
  }
}
