class Pessoas {
  String nome;
  int idade;
  String sexo;

  Pessoa1(String sexo, [int idade, String nome = 'Fulano']) {
    this.nome = nome;
    this.idade = idade;
    this.sexo = sexo;
  }

  Pessoa2({String nome, int idade, String sexo}) {
    this.nome = nome;
    this.idade = idade;
    this.sexo = sexo;
  }

  Escrever() {
    print('Nome: ${nome}, idade: ${idade}, sexo: ${sexo}');
  }
}

class Aula {
  String nome;
  String dia;

  Aula1(String dia, [String nome]) {
    this.nome = nome;
    this.dia = dia;
  }

  Aula2({String nome, String dia}) {
    this.nome = nome;
    this.dia = dia;
  }

  Escrever() {
    print('Nome = ${nome}, dia = ${dia}');
  }
}
