import 'package:flutter/material.dart';

class CardMusic extends StatelessWidget {
  final String titulo;
  final String subtitulo;
  CardMusic(this.titulo, this.subtitulo);

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 10,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          children: [
            Icon(Icons.music_note),
            Expanded(
              child: ListTile(
                  title: Text(this.titulo), subtitle: Text(this.subtitulo)),
            ),
          ],
        ),
      ),
    );
  }
}
