import 'package:flutter/material.dart';

class DefaultIcon extends StatefulWidget {
  final IconData icone;
  final String texto;
  final Function functionTap;
  final Function functionDoubleTap;
  const DefaultIcon(this.icone, this.texto,
      {this.functionTap, this.functionDoubleTap});

  @override
  _DefaultIconState createState() => _DefaultIconState();
}

class _DefaultIconState extends State<DefaultIcon> {
  Color cor;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    cor = Colors.blue;
  }

  @override
  Widget build(BuildContext context) {
    print('build');
    return Expanded(
      child: GestureDetector(
        onTap: widget.functionTap,
        onDoubleTap: widget.functionDoubleTap,
        child: Column(
          children: [
            Icon(
              widget.icone,
              color: cor,
            ),
            Text(
              widget.texto,
              style: TextStyle(color: cor),
            ),
          ],
        ),
      ),
    );
  }
}
