import 'package:aula09/maindartWidgets/defaultIcons.dart';
import 'package:flutter/material.dart';

import 'maindartWidgets/cardMusic.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  double containerHeight;
  Widget _myButton;
  @override
  void initState() {
    print('init');
    // TODO: implement initState
    super.initState();
    _myButton = FlatButton(
        onPressed: () => _changeFloatButton(),
        child: Icon(Icons.pregnant_woman));
  }

  _changeFloatButton() {
    setState(() {
      _myButton = Stack(
        children: [
          Text('teste'),
          Text('teste'),
        ],
      );
    });
  }

  List<Widget> _listCardMusic() {
    List<Widget> lista = [];
    for (int i = 0; i < 100; i++) {
      lista.add(CardMusic("Musica $i", "Autor $i"));
    }
    return lista;
  }

  @override
  Widget build(BuildContext context) {
    containerHeight = MediaQuery.of(context).size.height / 3;
    return Scaffold(
      appBar: AppBar(
        title: Text("Aula 09"),
      ),
      body: Column(
        children: [
          Text("Teste"),
          Expanded(
            child: ListView(
              shrinkWrap: true,
              children: _listCardMusic(),
            ),
          ),
        ],
      ) /* Container(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(15))),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              DefaultIcon(
                Icons.access_alarm,
                "Tete",
                functionTap: () => print('TAPPED'),
                functionDoubleTap: () => print('DOUBLE TAPPED'),
              ),
              DefaultIcon(Icons.account_box, "Tete")
            ],
          ),
        )); */
      ,
      floatingActionButton: _myButton,
    );
  }
}
