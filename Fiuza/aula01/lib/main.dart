import 'package:aula01/sensor.dart';
import 'package:flutter/material.dart';

import 'navegacao.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      initialRoute: '/initial',
      routes: {
        // When navigating to the "/" route, build the FirstScreen widget.
        '/initial': (context) => Aula08Full(),
        '/navegacao': (context) => Navegacao(),
        //'/': (context) => AppLifecycleReactor(),
        // When navigating to the "/second" route, build the SecondScreen widget.
      },
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
        // This makes the visual density adapt to the platform that you run
        // the app on. For desktop platforms, the controls will be smaller and
        // closer together (more dense) than on mobile platforms.
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      // home: Aula08(),
      // home: Aula08Full(),
    );
  }
}

class Aula08Full extends StatefulWidget {
  @override
  _Aula08FullState createState() {
    print("CREATED");
    return _Aula08FullState();
  }
}

class _Aula08FullState extends State<Aula08Full> {
  String resultado;
  TextEditingController num1 = TextEditingController();
  TextEditingController num2 = TextEditingController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    resultado = (DateTime.now().millisecondsSinceEpoch + 10).toString();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    print("FIM");
  }

  _navegaAi(BuildContext context) {
    //Navegação NOmeada
    //Navigator.of(context).pushNamed("/navegacao");
  }

  @override
  Widget build(BuildContext context) {
    print("BUILD");
    return Scaffold(
        floatingActionButton: FlatButton(
          shape: CircleBorder(),
          onPressed: () => this._navegaAi(context),
          child: Text("Navega"),
          color: Colors.amber,
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Image.asset('assets/caixa-generica.png'),
              /* Text(
            'Numero 1000',
          ), */
              TextFormField(
                controller: num1,
              ),
              Text(
                'Numero 24',
              ),
              TextFormField(
                controller: num2,
              ),
              FlatButton(
                child: Text("Somar"),
                onPressed: () {
                  int numUm = int.parse(num1.text);
                  int numDois = int.parse(num2.text);
                  int resultadoSoma = numUm + numDois;
                  setState(() {
                    print("SET STATE");
                    resultado = resultadoSoma.toString();
                  });
                },
              ),
              Text("Resultado = $resultado")
            ],
          ),
        ));
  }
}

class Aula08 extends StatelessWidget {
  String resultado;
  TextEditingController num1 = TextEditingController();
  TextEditingController num2 = TextEditingController();

  Aula08() {
    print("CONSTRUTOR");
  }
  @override
  Widget build(BuildContext context) {
    print("BUILD");
    return Scaffold(
        body: Center(
      // Center is a layout widget. It takes a single child and positions it
      // in the middle of the parent.
      child: Column(
        // Column is also a layout widget. It takes a list of children and
        // arranges them vertically. By default, it sizes itself to fit its
        // children horizontally, and tries to be as tall as its parent.
        //
        // Invoke "debug painting" (press "p" in the console, choose the
        // "Toggle Debug Paint" action from the Flutter Inspector in Android
        // Studio, or the "Toggle Debug Paint" command in Visual Studio Code)
        // to see the wireframe for each widget.
        //
        // Column has various properties to control how it sizes itself and
        // how it positions its children. Here we use mainAxisAlignment to
        // center the children vertically; the main axis here is the vertical
        // axis because Columns are vertical (the cross axis would be
        // horizontal).
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            'Numero 1',
          ),
          TextFormField(
            controller: num1,
          ),
          Text(
            'Numero 2',
          ),
          TextFormField(
            controller: num2,
          ),
          FlatButton(
            child: Text("Somar"),
            onPressed: () {
              int numUm = int.parse(num1.text);
              int numDois = int.parse(num2.text);
              int resultadoSoma = numUm + numDois;
              resultado = resultadoSoma.toString();
              print(resultado);
            },
          ),
          Text("Resultado = $resultado")
        ],
      ),
    ));
  }
}
/*
class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      // This call to setState tells the Flutter framework that something has
      // changed in this State, which causes it to rerun the build method below
      // so that the display can reflect the updated values. If we changed
      // _counter without calling setState(), then the build method would not be
      // called again, and so nothing would appear to happen.
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),
      ),
      body: Center(
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.
        child: Column(
          // Column is also a layout widget. It takes a list of children and
          // arranges them vertically. By default, it sizes itself to fit its
          // children horizontally, and tries to be as tall as its parent.
          //
          // Invoke "debug painting" (press "p" in the console, choose the
          // "Toggle Debug Paint" action from the Flutter Inspector in Android
          // Studio, or the "Toggle Debug Paint" command in Visual Studio Code)
          // to see the wireframe for each widget.
          //
          // Column has various properties to control how it sizes itself and
          // how it positions its children. Here we use mainAxisAlignment to
          // center the children vertically; the main axis here is the vertical
          // axis because Columns are vertical (the cross axis would be
          // horizontal).
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'You have pushed the button this many times:',
            ),
            Text(
              '$_counter',
              style: Theme.of(context).textTheme.headline4,
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
*/
