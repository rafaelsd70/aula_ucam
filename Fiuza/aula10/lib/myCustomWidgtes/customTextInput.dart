import 'package:flutter/material.dart';

class CustomTextInput extends StatelessWidget {
  final String texto;
  double tamanho = 18;
  CustomTextInput(this.texto, [this.tamanho]);
  @override
  Widget build(BuildContext context) {
    return Text(
      texto,
      style: TextStyle(fontSize: 18),
    );
  }
}
