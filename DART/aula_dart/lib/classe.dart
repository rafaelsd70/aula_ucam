class Aula {
  String nome;
  int dia;
  double x;

  Aula();
  MetodoSoParenteses(String nome, int dia) {
    this.nome = nome;
    this.dia = dia;
  }

  MetodoColchete(String nome, [int dia, double x = 0.5]) {
    this.nome = nome;
    this.dia = dia;
    this.x = x;
  }

  MetodoChaves({String nome, int dia, double x = 0.5}) {
    this.nome = nome;
    this.dia = dia;
    this.x = x;
  }

  Show() {
    print(this.nome + " dia " + this.dia.toString() + " " + this.x.toString());
  }
}
