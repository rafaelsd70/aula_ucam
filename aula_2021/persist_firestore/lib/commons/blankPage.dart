import 'package:flutter/cupertino.dart';

import 'package:flutter/material.dart';

// ignore: must_be_immutable
class CustomScaffold extends StatelessWidget {
  late GlobalKey _key;
  late String title;
  late Widget body;
  CustomScaffold(String title, Widget body) {
    this._key = new GlobalKey();
    this.title = title;
    this.body = body;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue,
        title: Text(this.title),
      ),
      body: this.body,
    );
  }
}
