import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/services.dart';
import 'package:persist_firestore/login.dart';

import 'commons/blankPage.dart';
import 'home.dart';

main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: (FirebaseAuth.instance.currentUser == null)
          ? LoginPage()
          : HomePage(),
    );
  }
}

/*

class MyHomePage extends StatefulWidget {
  MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int matricula = 0;
  final FirebaseFirestore firestore = FirebaseFirestore.instance;
  final FirebaseAuth auth = FirebaseAuth.instance;
  late String nome;
  late String email;
  late String senha;
  final GlobalKey<FormState> _form = GlobalKey<FormState>();

  @override
  initState() {
    super.initState();
  }

  _saveForm() {
    _form.currentState!.save();
    Map<String, dynamic> aluno = {
      "nome": nome,
      "matricula": matricula,
      "created_at": DateTime.now()
    };
    auth
        .createUserWithEmailAndPassword(email: this.email, password: this.senha)
        .then((value) {
      String userUid = value.user!.uid;
      firestore.collection("alunos").doc(userUid).set(aluno);
    }).catchError((onError) => print(onError));
    /*
    firestore
        .collection("alunos")
        .doc(matricula.toString())
        .set(aluno, SetOptions(merge: true));
        */
  }

  _checkLogin(context) {
    auth.authStateChanges().listen((user) {
      if (user == null) {
        Navigator.of(context)
            .push(MaterialPageRoute(builder: (context) => LoginPage()));
      }
    });
  }

  _logout() {
    auth.signOut();
  }

  @override
  Widget build(BuildContext context) {
    _checkLogin(context);
    return BankPage("Home", Container());
    /*
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue,
        title: Text("Cadastro aluno"),
      ),
      floatingActionButton:
          IconButton(onPressed: () => _logout(), icon: Icon(Icons.logout)),
      body: Container(
        padding: EdgeInsets.all(15),
        child: Form(
          key: _form,
          child: Column(
            children: [
              TextFormField(
                onSaved: (value) => nome = value!,
                decoration: InputDecoration(
                    hintText: "Insira o nome do aluno",
                    labelText: "Nome do caboclo",
                    helperText: "Não deixe em branco"),
              ),
              TextFormField(
                onSaved: (value) => matricula = int.parse(value!),
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                    hintText: "Insira matrícula do aluno",
                    labelText: "Numérica",
                    helperText: "Não deixe em branco"),
              ),
              TextFormField(
                onSaved: (value) => email = value!,
                keyboardType: TextInputType.emailAddress,
                decoration: InputDecoration(
                    hintText: "Informe o email",
                    helperText: "Não deixe em branco"),
              ),
              TextFormField(
                onSaved: (value) => senha = value!,
                obscureText: true,
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                    hintText: "Informe a senha",
                    labelText: "Numérica",
                    helperText: "Não deixe em branco"),
              ),
              TextButton(onPressed: () => _saveForm(), child: Text("Salvar")),
              Padding(padding: EdgeInsets.fromLTRB(0, 50, 0, 0)),
            ],
          ),
        ),
      ),
    ); */
  }
}
*/
